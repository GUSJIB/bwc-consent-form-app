import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { DatePipe } from '@angular/common';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { ContactViewPage } from '../pages/contact/contact.view';
import { DoctorConsentViewPage } from '../pages/DoctorConsent/DoctorConsent.view'
import { ConfirmPage } from '../pages/confirm/confirm'
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { TestFormsIndexPage } from '../pages/testforms/testforms.index';
import { FormsIndexPage } from '../pages/forms/forms.index';
import { FormsPage } from '../pages/forms/forms';
import { FormsViewPage } from '../pages/forms/forms.view';
import { TestFormsPage } from '../pages/testforms/testforms';
import { TestFormsViewPage } from '../pages/testforms/testforms.view';
import { FormsSearchPage } from '../pages/forms/forms.search';
import { TestFormsAttachPage } from '../pages/testforms/testforms.attach';
import { SignaturesPage } from '../pages/signatures/signatures';
import { SignaturesViewPage } from '../pages/signatures/signatures.view';
import { SignaturesAttachPage } from '../pages/signatures/signatures.attach';
import { SignaturesViewAttachPage } from '../pages/signatures/signatures.viewattach'; 
import { LoginPage } from '../pages/login/login';
import { SettingsPage } from '../pages/settings/settings';
import { UsersIndexPage } from '../pages/users/users.index';
import { UsersViewPage } from '../pages/users/users.view';

import { DocumentsPage } from '../pages/documents/documents';
import { DocumentsViewPage } from '../pages/documents/document.view';

import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';

import { HttpInterceptor } from '../providers/httpinterceptor';
import { ApiService } from '../providers/api-service/api-service';

import { ComponentsModule } from '../components/components.module';
import { RenderFormComponent } from '../components/render-form/render-form';
import { DrawPadComponent } from '../components/draw-pad/draw-pad';
import { CustomCheckboxComponent } from '../components/custom-checkbox/custom-checkbox';

import { SignaturePadModule } from 'angular2-signaturepad';
import { IonicStorageModule } from '@ionic/storage';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';

import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Base64 } from '@ionic-native/base64/ngx';

import { UploadService } from '../providers/upload-service/upload-service';

import { PipesModule  } from '../pipes/pipes.module';
import { ImagePipe } from '../pipes/image/image';

import { FormTemplatePage } from '../pages/form-template/form-template';

import { Platform } from 'ionic-angular';
import { HttpClientModule, HttpBackend, HttpXhrBackend } from '@angular/common/http';
import { NativeHttpModule, NativeHttpBackend, NativeHttpFallback } from 'ionic-native-http-connection-backend';

@NgModule({
  declarations: [   
    MyApp,
    AboutPage,
    ContactPage,
    ContactViewPage,
    HomePage,
    TabsPage,
    ConfirmPage,
    DoctorConsentViewPage,
    TestFormsPage,
    FormsIndexPage,
    TestFormsIndexPage,
    FormsViewPage,
    TestFormsViewPage,
    FormsSearchPage,
    TestFormsAttachPage,
    SignaturesPage,
    SignaturesViewPage,
    SignaturesAttachPage,
    SignaturesViewAttachPage,
    LoginPage,
    SettingsPage,
    UsersIndexPage,
    UsersViewPage,
    DocumentsPage,
    DocumentsViewPage,
    FormTemplatePage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp,{ mode: 'ios', tabsHideOnSubPages: true }, {
      links: [
         //{ component: LoginPage, name: 'Login', segment: 'login' },
      ]
    }),
    ComponentsModule,
    SignaturePadModule,
    PipesModule,
    IonicStorageModule.forRoot()
  ],
  exports: [SignaturePadModule],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    ContactViewPage,
    HomePage,
    TabsPage,
    FormsIndexPage,
    ConfirmPage,
    DoctorConsentViewPage,
    TestFormsIndexPage,
    FormsViewPage,
    TestFormsPage,
    TestFormsViewPage,
    FormsSearchPage,
    TestFormsAttachPage,
    SignaturesPage,
    SignaturesViewPage,
    SignaturesAttachPage,
    SignaturesViewAttachPage,
    LoginPage,
    SettingsPage,
    UsersIndexPage,
    UsersViewPage,
    DocumentsPage,
    DocumentsViewPage,
    FormTemplatePage
  ],
  providers: [
    FileTransfer, 
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    {provide: HttpBackend, useClass: NativeHttpFallback, deps: [Platform, NativeHttpBackend, HttpXhrBackend]},
    HttpInterceptor,
    ApiService,
    AuthServiceProvider,
    File,
    Base64,
    FileTransfer,
    Camera,
    UploadService,
    DatePipe
  ]
})
export class AppModule {}
