import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { DocumentsPage } from '../pages/documents/documents';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';

import { FormTemplatePage } from '../pages/form-template/form-template';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav, null) navCtrl: Nav;
  rootPage:any;

  constructor(private platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public events: Events, private authService: AuthServiceProvider) {
    this.events.subscribe('http:forbidden', error => {
      this.navCtrl.push(LoginPage);
    });
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();

      this.authService.checkAuthentication().then((res) => {
        if(!res) {
        //this.rootPage = LoginPage;
        this.rootPage = FormTemplatePage
        }else {
        //this.rootPage = TabsPage;
        this.rootPage = FormTemplatePage
        }
        this.splashScreen.hide();
      }, (err) => {
        this.rootPage = LoginPage;
        this.splashScreen.hide();
      })
    });
  }
}
