import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RenderFormComponent } from './render-form/render-form';
import { DrawPadComponent } from './draw-pad/draw-pad';
import { SignaturePadModule } from 'angular2-signaturepad';
import { IonicStorageModule } from '@ionic/storage';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { CustomCheckboxComponent } from './custom-checkbox/custom-checkbox';

@NgModule({
    declarations: [RenderFormComponent, 
    DrawPadComponent,
    CustomCheckboxComponent],
	imports: [BrowserModule, SignaturePadModule, IonicModule],
    exports: [RenderFormComponent,
    DrawPadComponent,
    CustomCheckboxComponent, SignaturePadModule]
})
export class ComponentsModule {}
