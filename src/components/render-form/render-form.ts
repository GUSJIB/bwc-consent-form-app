import { DrawPadComponent } from '../draw-pad/draw-pad';
import {
    Compiler,
    Component,
    ComponentFactory,
    ComponentFactoryResolver,
    ComponentRef,
    Directive,
    EventEmitter,
    FactoryProvider,
    Input,
    ModuleWithComponentFactories,
    NgModule,
    Output,
    ReflectiveInjector,
    RenderComponentType,
    ViewContainerRef,
    ViewEncapsulation,
} from '@angular/core';

import { AppModule } from '../../app/app.module';
import { CommonModule } from '@angular/common';
import { IonicModule, IonicErrorHandler } from 'ionic-angular';
import { ComponentsModule } from '../components.module';

export function createComponentFactory(compiler: Compiler, metadata: Component, metaMethod: any): Promise<ComponentFactory<any>> {
  // const cmpClass = class DynamicComponent {
  //   public handleEvent(childData:any){
  //     console.log(childData);
  //   }
  // };
  const decoratedCmp = Component(metadata)(metaMethod);

  @NgModule({ imports: [AppModule, CommonModule, ComponentsModule, IonicModule], declarations: [decoratedCmp] })
  class DynamicHtmlModule {}

  return compiler.compileModuleAndAllComponentsAsync(DynamicHtmlModule)
     .then((moduleWithComponentFactory: ModuleWithComponentFactories<any>) => {
      return moduleWithComponentFactory.componentFactories.find(x => x.componentType === decoratedCmp);
    });
}

@Directive({
  selector: 'render-form'
})
export class RenderFormComponent {
  @Input() html: string;
  @Input() customer: any;
  cmpRef: ComponentRef<any>;

  @Output('childData') outgoingData = new EventEmitter<string>();

  constructor(private vcRef: ViewContainerRef, private compiler: Compiler, private resolver: ComponentFactoryResolver) { }

  ngOnChanges() {
    const html = this.html;

    if (!html) return;

    if(this.cmpRef) {
      this.cmpRef.destroy();
    }

    const compMetadata = new Component({
        selector: 'dynamic-html',
        template: this.html
    });

    const compMetaClass = class DynamicComponent {
      outgoingData = new EventEmitter<string>();
      public handleEvent(childData:any){
        console.log(childData);
      }
    };

    createComponentFactory(this.compiler, compMetadata, compMetaClass)
      .then(factory => {
        // Inputs need to be in the following format to be resolved properly
        let inputProviders = Object.keys(this.customer).map((inputName) => {
          return {
            provide: inputName, useValue: this.customer[inputName]
          };
        });
        let resolvedInputs = ReflectiveInjector.resolve(inputProviders);
        const injector = ReflectiveInjector.fromResolvedProviders(resolvedInputs, this.vcRef.parentInjector);
        this.cmpRef = this.vcRef.createComponent(factory, 0, injector);
      });
  }

  ngOnDestroy() {
    if(this.cmpRef) {
      this.cmpRef.destroy();
    }
  }

}
