import { Component, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'custom-checkbox',
  templateUrl: 'custom-checkbox.html'
})
export class CustomCheckboxComponent {
  @ViewChild('myname', null) input:ElementRef;
  private checked: false;

  constructor(private myElement: ElementRef) {
    console.log('Hello CustomCheckboxComponent Component');
  }

  toggle(e) {
    console.log(e.target.checked);
    if(e.target.checked){
      this.input.nativeElement.setAttribute("checked", "checked");
    }else {
      this.input.nativeElement.removeAttribute("checked");
    }
    // if(!this.checked) {
    //   this.input.nativeElement.setAttribute("checked", "checked");
    // }else{
    //   this.input.nativeElement.removeAttribute("checked");
    // }
  }

}
