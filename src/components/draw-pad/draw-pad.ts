import { Component, ViewChild, Output, EventEmitter } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NavController, NavParams, Loading, LoadingController, ToastController } from 'ionic-angular';
import { ApiService } from '../../providers/api-service/api-service';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'draw-pad',
  templateUrl: 'draw-pad.html'
})
export class DrawPadComponent {

  @Output('childData') outgoingData = new EventEmitter<string>();

  signature = '';
  isDrawing = false;

  @ViewChild(SignaturePad, null) signaturePad: SignaturePad;
  private signaturePadOptions: Object = { // Check out https://github.com/szimek/signature_pad
    'minWidth': 0.5,
    'canvasWidth': 300,
    'canvasHeight': 100,
    'backgroundColor': '#f6fbff',
    'penColor': '#666a73'
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadCtrl: LoadingController,
    public apiService: ApiService,
    public storage: Storage,
    public toastCtrl: ToastController) {

  }

  ionViewDidEnter() {
    if(this.signaturePad) {
      this.signaturePad.clear();
    }
    // this.storage.get('savedSignature').then((data) => {
    //   this.signature = data;
    // });
  }

  drawComplete() {
    // this.isDrawing = false;
    // this.signature = this.signaturePad.toDataURL();
    // this.outgoingData.emit(this.signaturePad.toDataURL());
  }

  drawStart() {
    this.isDrawing = true;
  }

  savePad() {
    // this.signature = this.signaturePad.toDataURL();
    // this.storage.set('savedSignature', this.signature);
    // this.signaturePad.clear();
    // let toast = this.toastCtrl.create({
    //   message: 'New Signature saved.',
    //   duration: 3000,
    //   position: 'top'
    // });
    // toast.present();
    this.isDrawing = false;
    this.signature = this.signaturePad.toDataURL();
    this.outgoingData.emit(this.signaturePad.toDataURL());
  }

  clearPad() {
    this.signaturePad.clear();
  }

}
