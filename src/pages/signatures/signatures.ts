import { Component, ViewChild, ElementRef, ViewChildren } from '@angular/core';
import { NavController, NavParams, ModalController, LoadingController, Loading, ToastController } from 'ionic-angular';
import { ApiService } from '../../providers/api-service/api-service';
import { FormsSearchPage } from '../forms/forms.search';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common';
import * as jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';
import { File } from '@ionic-native/file';
@Component({
  selector: 'page-signatures',
  templateUrl: 'signatures.html',
})
export class SignaturesPage {

  directives: [SignaturePad];
  loading: Loading; 
  customer: any;
  template: any;

  elRef: ElementRef;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public loadCtrl: LoadingController,
    public apiService: ApiService,
    public storage: Storage,
    public toastCtrl: ToastController,
    private datePipe: DatePipe, 
    private file:File,  
    elRef: ElementRef) {
      this.elRef = elRef;
  }
  @ViewChild(SignaturePad, null) signaturePad: SignaturePad;
  private signaturePadOptions: Object = {
    'minWidth': 1,
    'canvasWidth': 744,
    'canvasHeight': 255,
    'backgroundColor': 'rgba(255, 255, 255, 0)',
    'penColor': 'rgb(0, 0, 0)'
  };
  private fileList = [];

  getHtmlContent() {
    // return this.elRef.nativeElement.innerHTML;
    return document.querySelector("page-signatures > ion-content > div.scroll-content").innerHTML;  
  }

  ionViewDidEnter() {
  }

  save() {
    this.generatePdf();
    // this.apiService.post("/signed/" + this.customer['id']  + "/" + this.template['id'], { paper: this.getHtmlContent() }).then((data) => {
    //   this.navCtrl.pop();
    // }, (err) => {
    //   console.log(err);
    // });
  }


  generatePdf(){
    //const div = document.getElementById("Html2Pdf");
    //const div = (document.getElementsByClassName("card card-ios")[2]) as HTMLElement;
    //  const list = document.getElementsByClassName("card card-ios");
    //  const list2 = document.getElementsByTagName("dynamic-html");
     //const div = document.getElementsByTagName("dynamic-html")[0] as HTMLElement;
     const div = document.getElementById("Html2Pdf");
     //console.log(list2);
    const options = {
      allowTaint: true ,
      useCORS: true,
      x : 0,
      y : 0,
     height:div.clientHeight,
     width:div.clientWidth,
     foreignObjectRendering: true,
     letterRendering: true,
    };
    console.log(div);
    html2canvas(div,options).then((canvas)=>{
      //Initialize JSPDF
      var doc = new jsPDF("p","mm","a4");
      //Converting canvas to Image
      let imgData = canvas.toDataURL("image/PNG");
      console.log(imgData);
      //Add image Canvas to PDF    
      doc.addImage(imgData, 'PNG', 0,0,doc.internal.pageSize.getWidth(),doc.internal.pageSize.getHeight());
      let pdfOutput = doc.output();
      // using ArrayBuffer will allow you to put image inside PDF
      let buffer = new ArrayBuffer(pdfOutput.length);
      let array = new Uint8Array(buffer);
      for (var i = 0; i < pdfOutput.length; i++) {
          array[i] = pdfOutput.charCodeAt(i);
      }
      let test = doc.output('datauristring');
      
      window.open(doc.output('bloburl'), '_blank');
  
      return false;
    });
  }

  saveattachment(documentId,file) {
    this.apiService.post("files/watermarkimage", { fileName: file['file'], remark: file['name'] }).then( (res) => {
      this.apiService.post("customer/" + this.customer['id'] + "/attachment", { fileName: file['name'], 
      expiryAt: file['expiredAt'], remark: file['remark'], documentId: documentId}).then((res) => {
     }, 
      (err) => {
        this.hideLoading();
        console.log(err);
      });
    }, (err) => {
      this.hideLoading();
      console.log(err);
    });
  }

  ionViewDidLoad() {
    this.customer = this.navParams.get("customer");
    let modalSelectForm = this.modalCtrl.create(FormsSearchPage, {}, { enableBackdropDismiss: false });
    modalSelectForm.onDidDismiss(data => {
      if(data != undefined) {
        this.loadDocument(data.id);
      }else {
        this.navCtrl.pop();
      }
      
    });
    modalSelectForm.present();
  }

  loadVisitSlip() {
    this.apiService.get('customer/' + this.customer['hn'] + '/today').then((res) => {
      this.template.details.body = this.template.details.body.replace(/{{physician}}/g, res['careprovName']);
      this.template.details.body = this.template.details.body.replace(/{{episodeDate}}/g, res['episodeDate']);
      this.template.details.body = this.template.details.body.replace(/{{location}}/g, res['locDesc']);
      this.hideLoading();
    }, (err) => {    
      console.log(err);
      this.hideLoading();
    });
  }

  loadDocument(id) {
    this.showLoading();
    this.apiService.get('document/' + id).then((res) => { 
      let dt = new Date();
      this.template = res;
      let dob = new Date(this.customer['dob']);
      this.customer['full_name'] =  [this.customer.titleTh, this.customer.firstnameTh, this.customer.middlenameTh, this.customer.lastnameTh].join(" ");
      this.template.details.body = this.template.details.body.replace(/{{full_name}}/g, this.customer['full_name']);
      this.template.details.body = this.template.details.body.replace(/{{age}}/g, this.customer['ageYear']);
      this.template.details.body = this.template.details.body.replace(/{{hn}}/g, this.customer['hn']);
      this.template.details.body = this.template.details.body.replace(/{{nationality}}/g, this.customer['nationality']);
      if(this.customer['idCard'] != "") {
        this.template.details.body = this.template.details.body.replace(/{{idcard}}/g, this.customer['idCard']);
      }
      if(this.customer['idCard'] == "" && this.customer['passportNumber'] != "") {
        this.template.details.body = this.template.details.body.replace(/{{idcard}}/g, this.customer['passportNumber']);
      }
      this.template.details.body = this.template.details.body.replace(/{{gender}}/g, this.customer['sex']);
      this.template.details.body = this.template.details.body.replace(/{{birthDate}}/g, this.datePipe.transform(dob, 'yyyy-MMM-dd'));
      this.template.details.body = this.template.details.body.replace(/\[\[datetime\]\]/g, dt.toLocaleString());
      this.template.details.body = this.template.details.body.replace(/{{title}}/g, res['details'].title);
      this.loadVisitSlip();
    }, (err) => {
      console.log(err);
      this.hideLoading();
    });
  }

  showLoading() {
    this.loading = this.loadCtrl.create({
      content: "Loading..." 
    });
    this.loading.present();
  }

  hideLoading() {
    this.loading.dismiss();
    let elm = document.getElementById('signaturepad');
    if(elm)
    {
      let elmR = new ElementRef(elm);
      this.signaturePad = new SignaturePad(elmR);
      this.signaturePad.options = this.signaturePadOptions;
      this.signaturePad.ngAfterContentInit();
      this.signaturePad.resizeCanvas();
      this.signaturePad.fromDataURL("http://10.90.10.203:8080/consentform_fileupload/Dental.png");
      this.signaturePad.ngAfterContentInit();
      this.signaturePad.resizeCanvas();
      var event = (event) => this.clear();
      var event2 = (event) => this.saveSignature();
      let button = document.getElementById('clearBTN').addEventListener('click', event);
      let button2 = document.getElementById('saveBTN').addEventListener('click', event2);
    }   
    // console.log(button);
  }

  drawComplete() {
    //this.signature = this.signaturePad.toDataURL();
  }

  drawStart() {
  }

  clear() {
    this.signaturePad.clear();
    this.signaturePad.fromDataURL("http://10.90.10.203:8080/consentform_fileupload/Dental.png");

  }

  
  addAttachment(item){
    let modalSelectForm = this.modalCtrl.create(SignaturesAttachPage, {item}, { enableBackdropDismiss: true });
    modalSelectForm.onDidDismiss(data => {
      if (item && data) {
        var index = this.fileList.indexOf(item);
        this.fileList[index] = data;
        // this.removeAttachment(item);
        // this.fileList.push(data);
      }
      else if(data) {
      this.fileList.push(data);
      }
    });
    modalSelectForm.present();
  }

  removeAttachment(item) {
    this.fileList = this.fileList.filter(x => x != item);
  } 

  ngAfterViewInit() {
    // @ViewChildren(SignaturePad) var signature:SignaturePad;
    // this.signaturePad = signature;
    // console.log(this.signaturePad);
    // console.log(signature); 
  }

  saveSignature(){
    //this.signature = this.signaturePad.toDataURL()
     //console.log(this.signaturePad.toDataURL("image/png"));
     console.log(this.signaturePad.toDataURL());
    }
}
