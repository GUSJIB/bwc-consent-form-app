import { Component,ViewChild, ElementRef } from '@angular/core';
import { ViewController, NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { ApiService } from '../../providers/api-service/api-service';
import { Camera, CameraOptions } from '@ionic-native/camera';
import * as watermark from 'watermarkjs';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { UploadService } from '../../providers/upload-service/upload-service';

@Component({
  selector: 'page-signatures-attatch',
  templateUrl: 'signatures.attach.html',
})
export class SignaturesAttachPage {

  loading: Loading;
  categories: any;
  minDate: any = new Date().getFullYear();
  maxDate: any = new Date().getFullYear() + 6;
  signature = '';
  expiryAt = '';
  remark = '';
  private base64Image: String;
  private resultImg: String;
  customer: any;
  private fileSelected: any;
  private existFile : any;

  constructor(public viewCtrl: ViewController, public navCtrl: NavController, private camera: Camera, public navParams: NavParams, public apiService: ApiService, public loadCtrl: LoadingController) {
    if(this.navParams.data.item) {
       this.existFile = this.navParams.data.item;
       this.expiryAt = this.existFile.expiredAt;
       this.remark = this.existFile.remark;
       this.base64Image = this.existFile.file;
       this.fileSelected = this.existFile.file;  
    }
  }

  @ViewChild('previewimage', null) waterMarkImage: ElementRef;
  OriginalImage = null;
  BlobImage = null;

  @ViewChild(SignaturePad, null) signaturePad: SignaturePad;
  private signaturePadOptions: Object = {
    'minWidth': 1,
    'canvasWidth': 992,
    'canvasHeight': 992,
    'backgroundColor': 'rgba(255, 255, 255, 0)',
    'penColor': 'rgb(0, 0, 0)'
  };


  ionViewDidLoad() {
    console.log('ionViewDidLoad TestFormsAttachPage');
  }

  select(item: any) {
    this.viewCtrl.dismiss(item);
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  showLoading() {
    this.loading = this.loadCtrl.create({
      content: "Loading..."
    });
    this.loading.present();
  }

  hideLoading() {
    this.loading.dismiss();
  }
  
  takePhoto() {

    const options: CameraOptions = {
      quality: 50,
      allowEdit: true,
      // destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.CAMERA,
      encodingType: this.camera.EncodingType.JPEG,
      saveToPhotoAlbum: false,
      correctOrientation:true
    }

    this.base64Image = "data:image/jpg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAoHBwgHBgoICAgLCgoLDhgQDg0NDh0VFhEYIx"+
    "8lJCIfIiEmKzcvJik0KSEiMEExNDk7Pj4+JS5ESUM8SDc9Pjv/2wBDAQoLCw4NDhwQEBw7KCIoOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O"+
    "zs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozv/wAARCAB7AKADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgED"+
    "AwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1h"+
    "ZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8v"+
    "P09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIF"+
    "EKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaX"+
    "mJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDXooor3jwAooooAKK"+
    "KKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACigUYoEJS0UmDQLYOlLRikxQGwtFFFBQUUfjRQAUUYNFIAooopgFFFFABRRR"+
    "QAUUUUAIKUZ9+PagVq+GgG1cA/8824rOcuSLkOEeZqJlfj0oq1qoA1S6AH/LQ1V7VUXzRTFJWbQfrQOK6XRpvs3h+e4CB2jZiAaq/8JRP/AM+kP"+
    "4GsPazbajHY29lBJOUjD/H9aX8q6S4W11nSJrtIBDPDnOPUc49xis3QLFL6+/fDMcS7mH970pxrrlcmthOi+ZJPcz0ikkBKRu4/2VJ/lTSCDggg"+
    "j161uT+JpklZLSCJIlOFDDnH4dKt28sPiK1limhWO5iGQ4/Qj2qfbTiuaUdClRg3aL1OYoo5BwQODR2NdRzhRRRQMKKKKACiiigAooooAT0rX8M"+
    "/8hgf9c2rIHatfwz/AMhgY/55tWNf+Gy6PxxKmrf8ha7/AOuhqoKt6r/yFbrn/loaqcDvVU/gQp/EzptFijm8OXEUsgiRmYM5PQVUGjaZn/kMR/"+
    "mtS2H/ACKd5/wKue47Vy04ylKVpW1OiclGMbq+h1N5btY6I8OnKJomyZZN2Tjuax9G1Iabd73UtG67Wx1FaPhdXWG6kfK22Bnd0zzn9KqaTptvq"+
    "Ud0Mt5sfMYB+uKS5YqUZ6ruU+aTjKJdbStJ1B2ktb4Rs3O3I/keap3Xh69s4zLDJ5qDrs+VsfTvWQylWKuNrLwQeorf8LT3DXUkRdngCZOTkA9v"+
    "61UlOlHmUromLhUlytWZgD1zn3pO2KnvAgvZwmNokbGPrUPUGuuLukzlaswoooqgCiiigAooooAKKKKAEFW9Nvm066FwqbztK4JxVUUlTKKkrMI"+
    "txd0bx8ShiSdOiJJ5JPX9Kjl8QLLE8f8AZ8K7lIyD0/Ssaisfq9Psa/WKnc1NO1ttPtDbfZlmUsSdzf0xVj/hI0x/yDIPz/8ArVhUtN0Kbd2hKv"+
    "NJK5pX2u3N5F5ICwxHqqdx6Z9Kp2l1NZTrPCxDDgjHBHoah/GjtVqnGMeVLQh1JN3vqbp16yuPnu9LV5MckYOfzqK48QN5JhsbZLVGHLL1/Csal"+
    "rNYemuho682A/GgUn+etLXQYoKKKKBhRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAB"+
    "RRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQB//Z"; 
    this.fileSelected = this.base64Image;
    this.addTextWatermark();

    // this.camera.getPicture(options).then((imageData) => {
    //   //imageData = imageData.replace(/^file:\/\//, '');
    //   // fetch(this.OriginalImage).then(res => res.blob()).then(blob =>{
    //   //     this.blob
    //   // });
    //   this.fileSelected = imageData;
    //   this.base64Image = imageData.replace(/^file:\/\//, '');
    //   this.addTextWatermark();
    // }, (err) => {
    //  console.log(err);
    //  if(err == "cordova_not_available") {
    //   console.log(err);
    //  }
    // });
  }

  save() {
    if(!this.existFile) {
        this.select({ 
          file: this.fileSelected, 
          name : Date.now().toString()+".png" ,
          remark : this.remark,
          expiredAt : this.expiryAt
      })
    }
    else this.select({
      file: this.fileSelected, 
      name : this.existFile.name,
      remark : this.remark,
      expiredAt : this.expiryAt
    });
  }

  addTextWatermark() {
    watermark([this.base64Image])
      .image(watermark.text.center('ใช้สำหรับ BDMS Wellness Clinic เท่านั้น', '90px Arial', '#B22222', 3.0)).render()
      .then((img) => {
        this.base64Image = img[0].src;
        this.fileSelected = img[0].src;
      });
  }

}
