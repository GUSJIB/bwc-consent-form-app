import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, ModalController, Loading, LoadingController, ToastController } from 'ionic-angular';
import { ApiService } from '../../providers/api-service/api-service';
import { FormsSearchPage } from '../forms/forms.search';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { Storage } from '@ionic/storage';
import { SignaturesViewAttachPage } from '../signatures/signatures.viewattach'

@Component({
  selector: 'page-signatures-view',
  templateUrl: 'signatures.view.html',
})
export class SignaturesViewPage {

  loading: Loading;
  document: any;
  customer: any;

  fileList: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public loadCtrl: LoadingController,
    public apiService: ApiService,
    public storage: Storage,
    public toastCtrl: ToastController) {
      this.document = this.navParams.get("document");
      this.customer = this.navParams.get("customer");
  }

  ionViewDidEnter() {
    this.apiService.get("files/attachments/" + this.document['id']).then( (res) => {
        this.fileList = res;
    });

  }

  ionViewDidLoad() {

  }

  showLoading() {
    this.loading = this.loadCtrl.create({
      content: "Loading..."
    });
    this.loading.present();
  }

  hideLoading() {
    this.loading.dismiss();
  }

   viewAttachment(item){
    let modalSelectForm = this.modalCtrl.create(SignaturesViewAttachPage, {item, customer: this.customer}, { enableBackdropDismiss: true });
    modalSelectForm.present();
  }

}
