import { Component,ViewChild, ElementRef } from '@angular/core';
import { ViewController, NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { ApiService } from '../../providers/api-service/api-service';
import { Camera, CameraOptions } from '@ionic-native/camera';
import * as watermark from 'watermarkjs';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { UploadService } from '../../providers/upload-service/upload-service';
import * as Constant from '../../configs/constants';

@Component({
  selector: 'page-signatures-viewattatch',
  templateUrl: 'signatures.viewattach.html',
})
export class SignaturesViewAttachPage { 

  loading: Loading;
  categories: any;
  minDate: any = new Date().getFullYear();
  maxDate: any = new Date().getFullYear() + 6;
  signature = '';
  expiryAt = '';
  remark = '';
  private base64Image: String;
  private resultImg: String;
  customer: any;
  private fileSelected: any;
  private existFile : any;

  constructor(public viewCtrl: ViewController, public navCtrl: NavController, private camera: Camera, public navParams: NavParams, public apiService: ApiService, public loadCtrl: LoadingController) {
    if(this.navParams.data.item) {
      this.existFile = this.navParams.data.item;
      this.customer = this.navParams.data.customer;
      this.expiryAt = this.existFile.expiryAt;
      this.remark = this.existFile.remark;
      // this.base64Image = this.existFile.file;
      // this.fileSelected = this.existFile.file;  
   }
  }

  @ViewChild('previewimage', null) waterMarkImage: ElementRef;
  OriginalImage = null;
  BlobImage = null;

  @ViewChild(SignaturePad, null) signaturePad: SignaturePad;
  private signaturePadOptions: Object = {
    'minWidth': 1,
    'canvasWidth': 992,
    'canvasHeight': 992,
    'backgroundColor': 'rgba(255, 255, 255, 0)',
    'penColor': 'rgb(0, 0, 0)'
  };


  ionViewDidLoad() {
    this.apiService.get("files/test/" + this.customer.id + "/" + this.existFile.id).then(res => {
      this.base64Image = "data:image/png;base64,"+ res['fileName'];
  });
  }

  select(item: any) {
    this.viewCtrl.dismiss(item);
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  showLoading() {
    this.loading = this.loadCtrl.create({
      content: "Loading..."
    });
    this.loading.present();
  }

  hideLoading() {
    this.loading.dismiss();
  }

  back() {
    this.viewCtrl.dismiss();
  }
}
