import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, App } from 'ionic-angular';
import { ApiService } from '../../providers/api-service/api-service';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { LoginPage } from '../login/login';
import { AboutPage } from '../about/about';
import { UsersIndexPage } from '../users/users.index';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private apiService: ApiService, public authService: AuthServiceProvider, public alertCtrl: AlertController, private _app: App) {
  }

  ionViewDidLoad() {
    this.apiService.get('users/profile').then((res) => {
      if(res['role'].alias == 'admin') {

      }
    });
  }

  logout() {
    let alert = this.alertCtrl.create({
      title: 'Confirm logout',
      message: 'Do you want to logout?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Logout',
          handler: () => {
            this.authService.logout().then((res) => {
              if(res){
                this._app.getRootNav().setRoot(LoginPage);
              }else{
                this.alertCtrl.create({
                  title: 'Fail!',
                  subTitle: 'Can\'t logout. Please try again.',
                  buttons: ['Dismiss']
                }).present();
              }
            });
          }
        }
      ]
    });
    alert.present();
  }

  users() {
    this.navCtrl.push(UsersIndexPage);
  }
  about(){
    this.navCtrl.push(AboutPage);
  }

}
