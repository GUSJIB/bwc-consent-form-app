import { Component,ViewChild, ElementRef } from '@angular/core';
import { ViewController, NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { ApiService } from '../../providers/api-service/api-service';
import { Camera, CameraOptions } from '@ionic-native/camera';

@Component({
  selector: 'page-confirm',
  templateUrl: 'confirm.html',
})
export class ConfirmPage {

  loading: Loading;

  constructor(public viewCtrl: ViewController, public navCtrl: NavController, private camera: Camera, public navParams: NavParams, public apiService: ApiService, public loadCtrl: LoadingController) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TestFormsAttachPage');
  }

  select(item: any) {
    this.viewCtrl.dismiss(item);
  }

  dismiss(answer: String) {
    this.viewCtrl.dismiss(answer);  
  }

  showLoading() {
    this.loading = this.loadCtrl.create({
      content: "Loading..."
    });
    this.loading.present();
  }

  hideLoading() {
    this.loading.dismiss();
  }
  

}