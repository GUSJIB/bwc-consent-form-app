import { ApiService } from '../../providers/api-service/api-service';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, Alert, AlertController, Loading, LoadingController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import * as watermark from 'watermarkjs';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { UploadService } from '../../providers/upload-service/upload-service';

@Component({
  selector: 'page-documents',
  templateUrl: 'documents.html',
})
export class DocumentsPage {

  minDate: any = new Date().getFullYear();
  maxDate: any = new Date().getFullYear() + 6;
  signature = '';
  expiryAt = '';
  remark = '';
  private base64Image: String;
  private resultImg: String;
  customer: any;
  private loading: Loading;
  private fileSelected: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private camera: Camera, public alertCtrl: AlertController, public uploadService: UploadService, public apiService: ApiService, public loadCtrl: LoadingController) {
    this.customer = this.navParams.get('customer');
  }

  @ViewChild('previewimage', null) waterMarkImage: ElementRef;
  OriginalImage = null;
  BlobImage = null;

  @ViewChild(SignaturePad, null) signaturePad: SignaturePad;
  private signaturePadOptions: Object = {
    'minWidth': 1,
    'canvasWidth': 992,
    'canvasHeight': 992,
    'backgroundColor': 'rgba(255, 255, 255, 0)',
    'penColor': 'rgb(0, 0, 0)'
  };

  ionViewDidLoad() {
  }

  drawComplete() {
    this.signature = this.signaturePad.toDataURL();
  }

  drawStart() {
  }

  clear() {
    this.signaturePad.clear();
  }

  takePhoto() {

    const options: CameraOptions = {
      quality: 50,
      allowEdit: true,
      // destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.CAMERA,
      encodingType: this.camera.EncodingType.JPEG,
      saveToPhotoAlbum: false,
      correctOrientation:true
    }

    this.camera.getPicture(options).then((imageData) => {
      //imageData = imageData.replace(/^file:\/\//, '');
      // fetch(this.OriginalImage).then(res => res.blob()).then(blob =>{
      //     this.blob
      // });
      this.fileSelected = imageData;
      this.base64Image = imageData.replace(/^file:\/\//, '');
      this.addTextWatermark();
    }, (err) => {
     console.log(err);
     if(err == "cordova_not_available") {
      this.alertCtrl.create({ 
        title: "Not Supported."
      }).present();
     }
    });
  }

  save() {
    this.showLoading();
    this.apiService.post("files/watermarkimage", { fileName: this.fileSelected }).then( (res) => {
    //this.uploadService.upload("files/upload", this.fileSelected, {}).then( (res) => {
      return this.apiService.post("files/signature", { remark: res['fileName'], fileName: this.signaturePad.toDataURL() });
    }, (err) => {
      console.log(err);
    }).then((res) => {
      console.log(res);
      return this.apiService.post("customer/" + this.customer['id'] + "/attachment", { fileName: res['fileName'], expiryAt: this.expiryAt, remark: this.remark });
    }, (err) => {
      console.log(err);
    }).then((res) => {
      this.hideLoading();
      this.navCtrl.pop();
    }, (err) => {
      console.log(err);
    });
  }

  showLoading() {
    this.loading = this.loadCtrl.create({
      content: "Loading..."
    });
    this.loading.present();
  }

  hideLoading() {
    this.loading.dismiss();
  }

  addTextWatermark() {
    watermark([this.base64Image])
      .image(watermark.text.center('ใช้สำหรับ BDMS Wellness Clinic เท่านั้น', '90px Arial', '#B22222', 3.0)).render()
      .then((img) => {
        this.base64Image = img[0].src;
        this.fileSelected = img[0].src;
      });
  }
}
