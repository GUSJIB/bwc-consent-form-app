import { ApiService } from '../../providers/api-service/api-service';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Alert, AlertController, Loading, LoadingController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { UploadService } from '../../providers/upload-service/upload-service';
import { Storage } from '@ionic/storage';
import * as Constant from '../../configs/constants';

@Component({
  selector: 'page-documents-view',
  templateUrl: 'document.view.html'
})
export class DocumentsViewPage {

  private loading: Loading;
  imgSrc = '';
  token: '';
  attachment: any;
  customer: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, private camera: Camera, public alertCtrl: AlertController, public uploadService: UploadService, public apiService: ApiService, public loadCtrl: LoadingController) {
    this.attachment = this.navParams.get("attachment");
    this.customer = this.navParams.get("customer");
    this.imgSrc = Constant.API_ENDPOINT + "files/" + this.customer.id + "/" + this.attachment.id;
    console.log(this.imgSrc);
    this.storage.get(Constant.API_TOKEN_KEY).then((value) => {
      this.token = value;
    });
  }

  ionViewDidLoad() {
  }

  ionViewDidEnter() {

  }

  showLoading() {
    this.loading = this.loadCtrl.create({
      content: "Loading..."
    });
    this.loading.present();
  }

  hideLoading() {
    this.loading.dismiss();
  }

}
