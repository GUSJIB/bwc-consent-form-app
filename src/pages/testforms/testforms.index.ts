import { Component, ViewChild, ElementRef, ViewChildren  } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, ToastController } from 'ionic-angular';
import { ApiService } from '../../providers/api-service/api-service';
import { TestFormsPage } from './testforms';
import { TestFormsViewPage } from './testforms.view';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'page-testforms-index',
  templateUrl: 'testforms.index.html',
})
export class TestFormsIndexPage {

  loading: Loading;
  templates: any;

  directives: [SignaturePad];
  template: any;

  elRef: ElementRef;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadCtrl: LoadingController,
    public apiService: ApiService,
    public storage: Storage,
    public toastCtrl: ToastController,
    private datePipe: DatePipe,    
    elRef: ElementRef)  {
      this.elRef = elRef;
  }


  @ViewChild(SignaturePad, null) signaturePad: SignaturePad;
  private signaturePadOptions: Object = {
    'minWidth': 1,
    'canvasWidth': 744,
    'canvasHeight': 255,
    'backgroundColor': 'rgba(255, 255, 255, 0)',
    'penColor': 'rgb(0, 0, 0)'
  };

  ionViewDidLoad() {
    this.showLoading();
    this.apiService.get("testdocuments").then((res) => {
      this.templates = res['content'];
      this.hideLoading();
    }, (err) => {

    });

  }

  view(item: any) {
    this.navCtrl.push(TestFormsViewPage, { id: item.id });
  }

  showLoading() {
    this.loading = this.loadCtrl.create({
      content: "Loading..."
    });
    this.loading.present();
  }

  hideLoading() {
    this.loading.dismiss();
  }

}
