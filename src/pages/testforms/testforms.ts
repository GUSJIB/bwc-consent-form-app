import { Component, ViewChild, ElementRef, ViewChildren } from '@angular/core';
import { NavController, NavParams, ModalController, LoadingController, Loading, ToastController } from 'ionic-angular';
import { ApiService } from '../../providers/api-service/api-service';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common';
import { delay } from 'rxjs/operator/delay';


@Component({
  selector: 'page-testforms',
  templateUrl: 'testforms.html',
})
export class TestFormsPage {

  directives: [SignaturePad];
  loading: Loading; 
  customer: any;
  document: any;

  elRef: ElementRef;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public loadCtrl: LoadingController,
    public apiService: ApiService,
    public storage: Storage,
    public toastCtrl: ToastController,
    private datePipe: DatePipe,    
    elRef: ElementRef) {
      this.elRef = elRef;
  }
  @ViewChild(SignaturePad, null) signaturePad: SignaturePad;
  private signaturePadOptions: Object = {
    'minWidth': 1,
    'canvasWidth': 744,
    'canvasHeight': 255,
    'backgroundColor': 'rgba(255, 255, 255, 0)',
    'penColor': 'rgb(0, 0, 0)'
  };

  getHtmlContent() {
    // return this.elRef.nativeElement.innerHTML;
    return document.querySelector("page-testforms > ion-content > div.scroll-content").innerHTML;
  }

  ionViewDidEnter() {
  }

  save() {
    this.apiService.post("/signed/" + this.customer['id']  + "/" + this.document['id'], { paper: this.getHtmlContent() }).then((data) => {
      this.navCtrl.pop();
    }, (err) => {
      console.log(err);
    });
  }

  ionViewDidLoad() {
    this.loadDocument(this.navParams.get('id'));
    // this.customer = this.navParams.get("customer");
    // let modalSelectForm = this.modalCtrl.create(FormsSearchPage, {}, { enableBackdropDismiss: false });
    // modalSelectForm.onDidDismiss(data => {
    //   if(data != undefined) {
    //   }else {
    //     this.navCtrl.pop();
    //   }
    // });
    // modalSelectForm.present();
  }

  loadVisitSlip() {
    this.apiService.get('customer/' + this.customer['hn'] + '/today').then((res) => {
      // this.template.details.body = this.template.details.body.replace(/{{physician}}/g, res['careprovName']);
      // this.template.details.body = this.template.details.body.replace(/{{episodeDate}}/g, res['episodeDate']);
      // this.template.details.body = this.template.details.body.replace(/{{location}}/g, res['locDesc']);
      this.hideLoading();
    }, (err) => {    
      console.log(err);
      this.hideLoading();
    });
  }

  loadDocument(id) {
    this.showLoading();
    this.apiService.get('testdocument/' + id).then((res) => { 
      //let dt = new Date();
      console.log(res);
      this.document = res;
      // let dob = new Date(this.customer['dob']);
      // this.customer['full_name'] =  [this.customer.titleTh, this.customer.firstnameTh, this.customer.middlenameTh, this.customer.lastnameTh].join(" ");
      // this.template.details.body = this.template.details.body .replace(/{{full_name}}/g, this.customer['full_name']);
      // this.template.details.body = this.template.details.body.replace(/{{age}}/g, this.customer['ageYear']);
      // this.template.details.body = this.template.details.body.replace(/{{hn}}/g, this.customer['hn']);
      // this.template.details.body = this.template.details.body.replace(/{{nationality}}/g, this.customer['nationality']);
      // if(this.customer['idCard'] != "") {
      //   this.template.details.body = this.template.details.body.replace(/{{idcard}}/g, this.customer['idCard']);
      // }
      // if(this.customer['idCard'] == "" && this.customer['passportNumber'] != "") {
      //   this.template.details.body = this.template.details.body.replace(/{{idcard}}/g, this.customer['passportNumber']);
      // }
      // this.template.details.body = this.template.details.body.replace(/{{gender}}/g, this.customer['sex']);
      // this.template.details.body = this.template.details.body.replace(/{{birthDate}}/g, this.datePipe.transform(dob, 'yyyy-MMM-dd'));
      // this.template.details.body = this.template.details.body.replace(/\[\[datetime\]\]/g, dt.toLocaleString());
      // this.template.details.body = this.template.details.body.replace(/{{title}}/g, res['details'].title);
      
      // this.loadVisitSlip();
      this.hideLoading();
    }, (err) => {
      console.log(err);
      this.hideLoading();
    });
  }

  showLoading() {
    this.loading = this.loadCtrl.create({
      content: "Loading..." 
    });
    this.loading.present();
  }

  hideLoading() {
    this.loading.dismiss();
    // let elm = document.getElementsByTagName('signature-pad')[0];
    // let elmR = new ElementRef(elm);
    // this.signaturePad = new SignaturePad(elmR);
    // this.signaturePad.options = this.signaturePadOptions;
    // this.signaturePad.ngAfterContentInit();
    // this.signaturePad.resizeCanvas();
    // this.signaturePad.fromDataURL("http://10.90.10.203:8080/consentform_fileupload/Dental.png");
    // this.signaturePad.ngAfterContentInit();
    // this.signaturePad.resizeCanvas();
    // var event = (event) => this.clear();
    // let button = document.getElementById('clearBTN').addEventListener('click', event);
    // console.log(button);
  }

  drawComplete() {
    //this.signature = this.signaturePad.toDataURL();
  }

  drawStart() {
  }

  clear() {
    this.signaturePad.clear();
    this.signaturePad.fromDataURL("http://10.90.10.203:8080/consentform_fileupload/Dental.png");

  }

  ngAfterViewInit() {
    // @ViewChildren(SignaturePad) var signature:SignaturePad;
    // this.signaturePad = signature;
    // console.log(this.signaturePad);
    // console.log(signature); 
  }
}
