import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef, ViewChildren } from '@angular/core';
import { NavController, NavParams, ModalController, LoadingController, Loading, ToastController } from 'ionic-angular';
import { ApiService } from '../../providers/api-service/api-service';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common';
import { Observable } from 'rxjs';
import { TestFormsAttachPage } from '../testforms/testforms.attach';

@Component({
  selector: 'page-testforms-view',
  templateUrl: 'testforms.view.html',
})
export class TestFormsViewPage {
  document: any;
  customer: any;
  directives: [SignaturePad];
  loading: Loading;

  elRef: ElementRef;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    public apiService: ApiService, 
    public storage: Storage,
    public toastCtrl: ToastController,
    public loadCtrl: LoadingController,
    public modalCtrl: ModalController,
    private datePipe: DatePipe,
    elRef: ElementRef   
    ) {
      this.elRef = elRef;
    }

  @ViewChild(SignaturePad, null) signaturePad: SignaturePad;
  private signaturePadOptions: Object = {
    'minWidth': 1,
    'canvasWidth': 744,
    'canvasHeight': 255,
    'backgroundColor': 'rgba(255, 255, 255, 0)',
    'penColor': 'rgb(0, 0, 0)'
  };
  
  private testfile: Object = {
    "name" : "test1.png",
    "file" : "data:image/jpg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAoHBwgHBgoICAgLCgoLDhgQDg0NDh0VFhEYIx"+
    "8lJCIfIiEmKzcvJik0KSEiMEExNDk7Pj4+JS5ESUM8SDc9Pjv/2wBDAQoLCw4NDhwQEBw7KCIoOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O"+
    "zs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozv/wAARCAB7AKADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgED"+
    "AwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1h"+
    "ZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8v"+
    "P09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIF"+
    "EKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaX"+
    "mJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDXooor3jwAooooAKK"+
    "KKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACigUYoEJS0UmDQLYOlLRikxQGwtFFFBQUUfjRQAUUYNFIAooopgFFFFABRRR"+
    "QAUUUUAIKUZ9+PagVq+GgG1cA/8824rOcuSLkOEeZqJlfj0oq1qoA1S6AH/LQ1V7VUXzRTFJWbQfrQOK6XRpvs3h+e4CB2jZiAaq/8JRP/AM+kP"+
    "4GsPazbajHY29lBJOUjD/H9aX8q6S4W11nSJrtIBDPDnOPUc49xis3QLFL6+/fDMcS7mH970pxrrlcmthOi+ZJPcz0ikkBKRu4/2VJ/lTSCDggg"+
    "j161uT+JpklZLSCJIlOFDDnH4dKt28sPiK1limhWO5iGQ4/Qj2qfbTiuaUdClRg3aL1OYoo5BwQODR2NdRzhRRRQMKKKKACiiigAooooAT0rX8M"+
    "/8hgf9c2rIHatfwz/AMhgY/55tWNf+Gy6PxxKmrf8ha7/AOuhqoKt6r/yFbrn/loaqcDvVU/gQp/EzptFijm8OXEUsgiRmYM5PQVUGjaZn/kMR/"+
    "mtS2H/ACKd5/wKue47Vy04ylKVpW1OiclGMbq+h1N5btY6I8OnKJomyZZN2Tjuax9G1Iabd73UtG67Wx1FaPhdXWG6kfK22Bnd0zzn9KqaTptvq"+
    "Ud0Mt5sfMYB+uKS5YqUZ6ruU+aTjKJdbStJ1B2ktb4Rs3O3I/keap3Xh69s4zLDJ5qDrs+VsfTvWQylWKuNrLwQeorf8LT3DXUkRdngCZOTkA9v"+
    "61UlOlHmUromLhUlytWZgD1zn3pO2KnvAgvZwmNokbGPrUPUGuuLukzlaswoooqgCiiigAooooAKKKKAEFW9Nvm066FwqbztK4JxVUUlTKKkrMI"+
    "txd0bx8ShiSdOiJJ5JPX9Kjl8QLLE8f8AZ8K7lIyD0/Ssaisfq9Psa/WKnc1NO1ttPtDbfZlmUsSdzf0xVj/hI0x/yDIPz/8ArVhUtN0Kbd2hKv"+
    "NJK5pX2u3N5F5ICwxHqqdx6Z9Kp2l1NZTrPCxDDgjHBHoah/GjtVqnGMeVLQh1JN3vqbp16yuPnu9LV5MckYOfzqK48QN5JhsbZLVGHLL1/Csal"+
    "rNYemuho682A/GgUn+etLXQYoKKKKBhRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAB"+
    "RRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQB//Z" 
  }

  private testfile2: Object = {
    "name" : "test2.png",
    "file" : "data:image/jpg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAoHBwgHBgoICAgLCgoLDhgQDg0NDh0VFhEYIx"+
    "8lJCIfIiEmKzcvJik0KSEiMEExNDk7Pj4+JS5ESUM8SDc9Pjv/2wBDAQoLCw4NDhwQEBw7KCIoOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O"+
    "zs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozv/wAARCAB7AKADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgED"+
    "AwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1h"+
    "ZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8v"+
    "P09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIF"+
    "EKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaX"+
    "mJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDXooor3jwAooooAKK"+
    "KKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACigUYoEJS0UmDQLYOlLRikxQGwtFFFBQUUfjRQAUUYNFIAooopgFFFFABRRR"+
    "QAUUUUAIKUZ9+PagVq+GgG1cA/8824rOcuSLkOEeZqJlfj0oq1qoA1S6AH/LQ1V7VUXzRTFJWbQfrQOK6XRpvs3h+e4CB2jZiAaq/8JRP/AM+kP"+
    "4GsPazbajHY29lBJOUjD/H9aX8q6S4W11nSJrtIBDPDnOPUc49xis3QLFL6+/fDMcS7mH970pxrrlcmthOi+ZJPcz0ikkBKRu4/2VJ/lTSCDggg"+
    "j161uT+JpklZLSCJIlOFDDnH4dKt28sPiK1limhWO5iGQ4/Qj2qfbTiuaUdClRg3aL1OYoo5BwQODR2NdRzhRRRQMKKKKACiiigAooooAT0rX8M"+
    "/8hgf9c2rIHatfwz/AMhgY/55tWNf+Gy6PxxKmrf8ha7/AOuhqoKt6r/yFbrn/loaqcDvVU/gQp/EzptFijm8OXEUsgiRmYM5PQVUGjaZn/kMR/"+
    "mtS2H/ACKd5/wKue47Vy04ylKVpW1OiclGMbq+h1N5btY6I8OnKJomyZZN2Tjuax9G1Iabd73UtG67Wx1FaPhdXWG6kfK22Bnd0zzn9KqaTptvq"+
    "Ud0Mt5sfMYB+uKS5YqUZ6ruU+aTjKJdbStJ1B2ktb4Rs3O3I/keap3Xh69s4zLDJ5qDrs+VsfTvWQylWKuNrLwQeorf8LT3DXUkRdngCZOTkA9v"+
    "61UlOlHmUromLhUlytWZgD1zn3pO2KnvAgvZwmNokbGPrUPUGuuLukzlaswoooqgCiiigAooooAKKKKAEFW9Nvm066FwqbztK4JxVUUlTKKkrMI"+
    "txd0bx8ShiSdOiJJ5JPX9Kjl8QLLE8f8AZ8K7lIyD0/Ssaisfq9Psa/WKnc1NO1ttPtDbfZlmUsSdzf0xVj/hI0x/yDIPz/8ArVhUtN0Kbd2hKv"+
    "NJK5pX2u3N5F5ICwxHqqdx6Z9Kp2l1NZTrPCxDDgjHBHoah/GjtVqnGMeVLQh1JN3vqbp16yuPnu9LV5MckYOfzqK48QN5JhsbZLVGHLL1/Csal"+
    "rNYemuho682A/GgUn+etLXQYoKKKKBhRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAB"+
    "RRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQB//Z" 
  }

  private fileList = [];

  ngOnInit(){   
  }

  ionViewDidLoad() {
        this.showLoading();
        this.apiService.get('customer/8117000304').then((res) => {
        this.customer = res;
        this.loadDocument(this.navParams.get('id'));
    }, (err) => {    
      console.log(err);
      this.hideLoading();
    });
  }

  ionViewDidEnter() {
    this.fileList.push(this.testfile);
    this.fileList.push(this.testfile2);
  }

  loadVisitSlip() {
    this.apiService.get('customer/8117000304').then((res) => {
      console.log("1");
      this.document.details.body = this.document.details.body.replace(/{{physician}}/g, res['careprovName']);
      this.document.details.body = this.document.details.body.replace(/{{episodeDate}}/g, res['episodeDate']);
      this.document.details.body = this.document.details.body.replace(/{{location}}/g, res['locDesc']);
      console.log("1"); 
      this.hideLoading();
    }, (err) => {    
      console.log(err);
      this.hideLoading();
    });
  }

  loadDocument(id) {
    this.apiService.get('testdocument/' + id).then((res) => { 
      let dt = new Date();
      this.document = res;
      console.log(this.customer);
      let dob = new Date(this.customer['dob']);
      this.customer['full_name'] =  [this.customer.titleTh, this.customer.firstnameTh, this.customer.middlenameTh, this.customer.lastnameTh].join(" ");
      this.document.details.body = this.document.details.body .replace(/{{full_name}}/g, this.customer['full_name']);
      this.document.details.body = this.document.details.body.replace(/{{age}}/g, this.customer['ageYear']);
      this.document.details.body = this.document.details.body.replace(/{{hn}}/g, this.customer['hn']);
      this.document.details.body = this.document.details.body.replace(/{{nationality}}/g, this.customer['nationality']);
      if(this.customer['idCard'] != "") {
        this.document.details.body = this.document.details.body.replace(/{{idcard}}/g, this.customer['idCard']);
      }
      if(this.customer['idCard'] == "" && this.customer['passportNumber'] != "") {
        this.document.details.body = this.document.details.body.replace(/{{idcard}}/g, this.customer['passportNumber']);
      }
      this.document.details.body = this.document.details.body.replace(/{{gender}}/g, this.customer['sex']);
      this.document.details.body = this.document.details.body.replace(/{{birthDate}}/g, this.datePipe.transform(dob, 'yyyy-MMM-dd'));
      this.document.details.body = this.document.details.body.replace(/\[\[datetime\]\]/g, dt.toLocaleString());
      this.document.details.body = this.document.details.body.replace(/{{title}}/g, res['details'].title);
      this.loadVisitSlip();
    }, (err) => {
      console.log(err);
      this.hideLoading();
    });
  }

  showLoading() {
    this.loading = this.loadCtrl.create({
      content: "Loading..."
    });
    this.loading.present();
  }

  hideLoading() {
    this.loading.dismiss();
  }

  addAttachment(item){
    let modalSelectForm = this.modalCtrl.create(TestFormsAttachPage, {item}, { enableBackdropDismiss: true });
    modalSelectForm.onDidDismiss(data => {
      if(data) {
      this.fileList.push(data);
      }
    });
    modalSelectForm.present();
  }

  removeAttachment(item) {
    this.fileList = this.fileList.filter(x => x != item);
  } 

}
