import { Component } from '@angular/core';
import { NavController, LoadingController, Loading, App } from 'ionic-angular';
import { ApiService } from '../../providers/api-service/api-service';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ContactViewPage } from './contact.view';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  loading: Loading;
  items: any;
  customers: any;

  constructor(public navCtrl: NavController, public apiService: ApiService, public authService: AuthServiceProvider, public loadCtrl: LoadingController, private _app: App) {

  }

  ionViewDidLoad() {
    this.showLoading();
    this.apiService.get('customers').then((res) => {
      this.items = res['content'];
      this.initializeItems();
      this.hideLoading();
    }, (err) => {
      console.log(err);
      this.hideLoading();
      this.authService.logout().then((res) => {
        if(res){
          this._app.getRootNav().setRoot(LoginPage);
        }
      });
    })
  }

  initializeItems() {
    this.customers = this.items;
  }

  findCustomer(ev: any) {
    this.initializeItems();
    let val = ev.target.value;
    if (val && val.trim() != '' && val.length >= 5) {
      this.apiService.get('customers?q=' + val).then((data) => {
        this.customers = data['content'];
      }, (error) => {
        this.hideLoading();
      });
    }
  }

  viewCustomer(item) {
    this.navCtrl.push(ContactViewPage, { hn: item.hn });
  }

  doRefresh(refresher) {
    this.apiService.get('customers').then((res) => {
      this.items = res['content'];
      this.initializeItems();
      refresher.complete();
      this.hideLoading();
    }, (err) => {
      console.log(err);
      this.hideLoading();
    });
  }

  showLoading() {
    this.loading = this.loadCtrl.create({
      content: "Loading..."
    });
    this.loading.present();
  }

  hideLoading() {
    this.loading.dismiss();
  }

}
