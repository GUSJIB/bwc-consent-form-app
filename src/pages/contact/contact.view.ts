import { Component } from '@angular/core';
import { Loading, LoadingController, NavController, NavParams } from 'ionic-angular';
import { ApiService } from '../../providers/api-service/api-service';
import { SignaturesPage } from '../signatures/signatures';
import { SignaturesViewPage } from '../signatures/signatures.view';
import { DocumentsPage } from '../documents/documents';
import { DocumentsViewPage } from '../documents/document.view';
import { DoctorConsentViewPage } from '../DoctorConsent/DoctorConsent.view'

@Component({
  selector: 'page-view-contact',
  templateUrl: 'contact.view.html'
})
export class ContactViewPage {

  loading: Loading;
  customer: any;
  private segment: String = 'forms';

  constructor(public navCtrl: NavController, public navPrams: NavParams, public apiService: ApiService, public loadCtrl: LoadingController) {

  }

  initial() {
    this.showLoading();
    this.apiService.get('customer/' + this.navPrams.get('hn')).then((res) => {
      this.customer = res;
      console.log(this.customer);
      this.hideLoading();
    }, (err) => {
      console.log(err);
      this.hideLoading();
    })
  }

  ionViewDidLoad() {
    // this.initial();
  }

  ionViewDidEnter() {
    this.initial();
  }

  viewSigned(item) {
    this.navCtrl.push(SignaturesViewPage, { document: item, customer: this.customer });
  }

  viewAttachment(item: any) {
    this.navCtrl.push(DocumentsViewPage, { attachment: item, customer: this.customer });
  }

  openDoctorConsent(item: any) {
    this.navCtrl.push(DoctorConsentViewPage, {   });
  }

  newConsent(item) {
    this.navCtrl.push(SignaturesPage, { customer: item });
  }

  newAttachment(item) {
    this.navCtrl.push(DocumentsPage, { customer: item });
  }

  showLoading() {
    this.loading = this.loadCtrl.create({
      content: "Loading..."
    });
    this.loading.present();
  }

  hideLoading() {
    this.loading.dismiss();
  }

}
