import { Component } from '@angular/core';
import { NavController, NavParams, Loading, LoadingController } from 'ionic-angular';
import { ApiService } from '../../providers/api-service/api-service';

@Component({
  selector: 'page-users-view',
  templateUrl: 'users.view.html',
})
export class UsersViewPage {

  loading: Loading;
  private user: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public apiService: ApiService, public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {

  }

  ionViewDidEnter() {
    this.user = this.navParams.get('user');
    console.log(this.user);
  }

  save() {
    console.log(this.user);
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Loading..."
    });
    this.loading.present();
  }
  hideLoading() {
    this.loading.dismiss();
  }

}
