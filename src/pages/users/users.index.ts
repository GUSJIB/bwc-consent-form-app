import { Component } from '@angular/core';
import { NavController, NavParams, Loading, LoadingController } from 'ionic-angular';
import { ApiService } from '../../providers/api-service/api-service';
import { UsersViewPage } from './users.view';

@Component({
  selector: 'page-users-index',
  templateUrl: 'users.index.html',
})
export class UsersIndexPage {

  loading: Loading;
  private users: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public apiService: ApiService, public loadingCtrl: LoadingController) {
    this.showLoading();
  }

  ionViewDidLoad() {

  }

  ionViewDidEnter() {
    this.apiService.get("users").then((res) => {
      this.users = res;
      this.hideLoading();
    }, (err) => {
      this.hideLoading();
    });
  }

  viewUser(user: any) {
    this.navCtrl.push(UsersViewPage, { user: user });
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Loading..."
    });
    this.loading.present();
  }
  hideLoading() {
    this.loading.dismiss();
  }

}
