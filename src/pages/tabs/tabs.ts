import { Component } from '@angular/core';
import { ApiService } from '../../providers/api-service/api-service';
import { SettingsPage } from '../settings/settings';
import { ContactPage } from '../contact/contact';
import { FormsIndexPage } from '../forms/forms.index';
import { TestFormsPage } from '../testforms/testforms';
import { TestFormsIndexPage } from '../testforms/testforms.index';
import { UsersIndexPage } from '../users/users.index';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = ContactPage;
  tab2Root = FormsIndexPage;
  tabxRoot = TestFormsIndexPage;
  tab3Root = SettingsPage

  constructor(private apiService: ApiService) {

  }
}
