import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, Keyboard } from 'ionic-angular';
//import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { Base64 } from '@ionic-native/base64/ngx';

import * as jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';
//import * as html2canvas from 'html2canvas-render-offscreen';
import { File } from '@ionic-native/file';
import { toBase64String } from '@angular/compiler/src/output/source_map';

@Component({
  selector: 'page-form-template',
  templateUrl: 'form-template.html',
})
export class FormTemplatePage {
  signature = '';
  constructor(public navCtrl: NavController, public navParams: NavParams, private file:File) {
  }
  // @ViewChild(SignaturePad) signaturePad: SignaturePad;
  // private signaturePadOptions: Object = {
  //   'minWidth': 1,
  //   'canvasWidth': 744,
  //   'canvasHeight': 255,
  //   'backgroundColor': 'rgba(255, 255, 255, 0)',
  //   'penColor': 'rgb(0, 0, 0)'
  // };
  //private signaturePadOptions: Object;

  ionViewDidLoad() {
    // if (this.signaturePad) {
    //   // var img = new Image();
    //   // img.setAttribute('crossOrigin', 'anonymous');
    //   // img.src = "http://10.90.10.203:8080/consentform_fileupload/Dental.png";
    //   // var Canvas = document.getElementsByTagName('canvas')[0];
    //   // var ctx = Canvas.getContext("2d");
    //   // ctx.drawImage(img,0,0);
      
    // //this.signaturePad.fromDataURL("http://10.90.10.203:8080/consentform_fileupload/Dental.png");
    // }
    console.log('ionViewDidLoad FormTemplatePage');
  }
  drawComplete() {
    //this.signature = this.signaturePad.toDataURL();
  }

  drawStart() {
  }

  clear() {
    //this.signaturePad.clear();
    //this.signaturePad.fromDataURL("http://10.90.10.203:8080/consentform_fileupload/Dental.png");
  }

  save(){
    //this.signature = this.signaturePad.toDataURL()
     //console.log(this.signaturePad);
  }

  generatePdf(){
    
    const div = document.getElementById("Html2Pdf");

    const options = {
      x : 0,
      y : 0,
    height:div.clientHeight,
    width:div.clientWidth,
     foreignObjectRendering: true,
     letterRendering: false
    };
    console.log(div);
    html2canvas(div,options).then((canvas)=>{
      //document.body.appendChild(canvas);
      //Initialize JSPDF
      var doc = new jsPDF("p","mm","a4");
      //doc.addHTML(canvas,0,0);
      //Converting canvas to Image
      let imgData = canvas.toDataURL("image/PNG");
      console.log(imgData);
      //Add image Canvas to PDF
      
      doc.addImage(imgData, 'PNG', 0,0,doc.internal.pageSize.getWidth(),doc.internal.pageSize.getHeight());
      let pdfOutput = doc.output();
      // using ArrayBuffer will allow you to put image inside PDF
      let buffer = new ArrayBuffer(pdfOutput.length);
      let array = new Uint8Array(buffer);
      for (var i = 0; i < pdfOutput.length; i++) {
          array[i] = pdfOutput.charCodeAt(i);
      }
      let test = doc.output('datauristring');
      
      window.open(doc.output('bloburl'), '_blank');
      //window.open(test); 
      //var text = toBase64String(data);
      // var url = 'data:application/pdf;base64,' + this.base64.encodeFile(data);
      // document.location.href = url;
      //This is where the PDF file will stored , you can change it as you like
      // for more information please visit https://ionicframework.com/docs/native/file/
      //const directory = this.file.externalApplicationStorageDirectory ;
      //Name of pdf
      const fileName = "example.pdf";
    //   this.file.resolveDirectoryUrl(directory).then((data) => {
      
    //   //Writing File to Device
    //   //this.file.writeFile(directory,fileName,buffer)
    //   this.file.writeFile( data.toURL(),fileName,buffer)
    //   .then((success)=> console.log("File created Succesfully" + JSON.stringify(success)))
    //   .catch((error)=> console.log("Cannot Create File " +JSON.stringify(error)));
    //   });
      return false;
    });
  }

} 