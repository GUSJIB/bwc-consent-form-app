import { Component, OnInit, ViewEncapsulation,ViewChild } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { ApiService } from '../../providers/api-service/api-service';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';


@Component({
  selector: 'page-view-forms',
  templateUrl: 'forms.view.html',
})
export class FormsViewPage implements OnInit {

  loading: Loading;
  document: any;
  customer: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public apiService: ApiService, public loadCtrl: LoadingController) {
  }

  @ViewChild(SignaturePad, null) signaturePad: SignaturePad;
  private signaturePadOptions: Object = {
    'minWidth': 1,
    'canvasWidth': 744,
    'canvasHeight': 255,
    'backgroundColor': 'rgba(255, 255, 255, 0)',
    'penColor': 'rgb(0, 0, 0)'
  };


  ngOnInit(){
    this.showLoading();
    this.apiService.get("document/" + this.navParams.get('id')).then((res) => {
      this.document = res;
      this.hideLoading();
    }, (err) => {
      this.hideLoading();
    });
  }

  ionViewDidLoad() {

  }

  showLoading() {
    this.loading = this.loadCtrl.create({
      content: "Loading..."
    });
    this.loading.present();
  }

  hideLoading() {
    this.loading.dismiss();
  }

}
