import { Component } from '@angular/core';
import { ViewController, NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { ApiService } from '../../providers/api-service/api-service';

@Component({
  selector: 'page-search-forms',
  templateUrl: 'forms.search.html',
})
export class FormsSearchPage {

  loading: Loading;
  categories: any;

  constructor(public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams, public apiService: ApiService, public loadCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FormsSearchPage');
    this.showLoading();
    this.apiService.get("documents").then((res) => {
      this.categories = res['content'];
      this.hideLoading();
    }, (err) => {

    });

  }

  select(item: any) {
    this.viewCtrl.dismiss(item);
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  showLoading() {
    this.loading = this.loadCtrl.create({
      content: "Loading..."
    });
    this.loading.present();
  }

  hideLoading() {
    this.loading.dismiss();
  }
}
