import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { ApiService } from '../../providers/api-service/api-service';
import { FormsViewPage } from './forms.view';

@Component({
  selector: 'page-index-forms',
  templateUrl: 'forms.index.html',
})
export class FormsIndexPage {

  loading: Loading;
  categories: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public apiService: ApiService, public loadCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    this.showLoading();
    this.apiService.get("documents").then((res) => {
      this.categories = res['content'];
      this.hideLoading();
    }, (err) => {

    });

  }

  view(item: any) {
    this.navCtrl.push(FormsViewPage, { id: item.id });
  }

  showLoading() {
    this.loading = this.loadCtrl.create({
      content: "Loading..."
    });
    this.loading.present();
  }

  hideLoading() {
    this.loading.dismiss();
  }

}
