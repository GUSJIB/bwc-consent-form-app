import { Component, ViewChild, ElementRef, ViewChildren } from '@angular/core';
import { NavController, NavParams, ModalController, LoadingController, Loading, ToastController } from 'ionic-angular';
import { ApiService } from '../../providers/api-service/api-service';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common';
import { ConfirmPage } from '../confirm/confirm';
import { AlertController } from 'ionic-angular';

@Component({
  selector: 'page-doctorconsent-view',
  templateUrl: 'doctorconsent.view.html',
})
export class DoctorConsentViewPage {

  directives: [SignaturePad];
  loading: Loading; 
  customer: any;
  template: any;

  elRef: ElementRef;

  constructor(
    private alertCtrl: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public loadCtrl: LoadingController,
    public apiService: ApiService,
    public storage: Storage,
    public toastCtrl: ToastController,
    private datePipe: DatePipe,    
    elRef: ElementRef) {
      this.elRef = elRef;
  }
  @ViewChild(SignaturePad, null) signaturePad: SignaturePad;
  private signaturePadOptions: Object = {
    'minWidth': 1,
    'canvasWidth': 744,
    'canvasHeight': 255,
    'backgroundColor': 'rgba(255, 255, 255, 0)',
    'penColor': 'rgb(0, 0, 0)'
  };
  private fileList = [];

  ionViewDidEnter() {
  }

  save() {

    //  this.fileList.forEach(file => {      
    //     this.saveattachment(102,file);
    //   });

    this.apiService.post("/signed/" + this.customer['id']  + "/" + this.template['id'],{}).then((data) => {

      this.fileList.forEach(file => {
        
        // this.saveattachment(data['id'],file);
      });
      this.navCtrl.pop();
      this.hideLoading();
    }, (err) => {
      console.log(err);
    });
  }

  ionViewDidLoad() {

  }

  getConfirm() {
    let modalSelectForm = this.modalCtrl.create(ConfirmPage, {}, { enableBackdropDismiss: false });
    modalSelectForm.onDidDismiss(data => {
      if(data != undefined) {
        // this.loadDocument(data.id);
      }else {
        this.navCtrl.pop();
      }
      
    });
    modalSelectForm.present();
  }

  showLoading() {
    this.loading = this.loadCtrl.create({
      content: "Loading..." 
    });
    this.loading.present();
  }

  hideLoading() {
    this.loading.dismiss();

    // let elm = document.getElementsByTagName('signature-pad')[0];
    // let elmR = new ElementRef(elm);
    // this.signaturePad = new SignaturePad(elmR);
    // this.signaturePad.options = this.signaturePadOptions;
    // this.signaturePad.ngAfterContentInit();
    // this.signaturePad.resizeCanvas();
    // this.signaturePad.fromDataURL("http://10.90.10.203:8080/consentform_fileupload/Dental.png");
    // this.signaturePad.ngAfterContentInit();
    // this.signaturePad.resizeCanvas();
    // var event = (event) => this.clear();
    // let button = document.getElementById('clearBTN').addEventListener('click', event);
    // console.log(button);
  }

  drawComplete() {
    //this.signature = this.signaturePad.toDataURL();
  }

  drawStart() {
  }

  ngAfterViewInit() {

  }

  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Confirm Sign?',
      message: 'Confirm Signing this document?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log('Buy clicked');
          }
        }
      ]
    });
    alert.present();
  }

}
