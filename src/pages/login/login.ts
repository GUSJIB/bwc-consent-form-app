import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController, Alert } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  private loading: Loading;
  private username: String;
  private password: String;
  constructor(public navCtrl: NavController, public navParams: NavParams, private authService: AuthServiceProvider, public loadingCtrl: LoadingController, private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    this.loading = this.loadingCtrl.create({
      content: "Authenticating..."
    });
  }

  login() {
    this.showLoading();

    this.authService.signup({username: this.username, password: this.password}).then((res) => {
      if(res['active']) {
        this.authService.login({username: this.username, password: this.password}).then((res) => {
          console.log(res);
          this.navCtrl.push(TabsPage);
          this.hideLoading();
        }, (err) => {
          this.alertCtrl.create({
            title: 'Fail.',
            subTitle: 'Password not match. Please try again.',
            buttons: ['Dismiss']
          }).present();
          this.hideLoading();
        });
      }else {
        this.loading.dismiss();

        let loading = this.loadingCtrl.create({
          content: "Authenticating..."
        });

        let alert = this.alertCtrl.create({
          title: 'Confirm Password',
          inputs: [
            {
              name: 'password',
              placeholder: 'Password',
              type: 'password'
            }
          ],
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel',
              handler: data => {

              }
            },
            {
              text: 'Confirm',
              handler: data => {

                // begin the alert's dismiss transition
                let navTransition = alert.dismiss();

                navTransition.then(() => {
                  loading.present();
                  this.authService.confirmPassword({username: this.username, password: data.password}).then((res) => {
                    if(res['active']) {
                      this.authService.login({username: this.username, password: data.password}).then((res) => {
                          loading.dismiss();
                          this.navCtrl.push(TabsPage);
                        }, (err) => {

                        });
                      }else {
                        this.alertCtrl.create({
                          title: 'Fail.',
                          subTitle: 'Password not match. Please try again.',
                          buttons: ['Dismiss']
                        }).present();
                        return false;
                      }
                    });
                });
                return false;
              }
            }
          ]
        });
        alert.present();
      }
    }, (err) => {
      this.hideLoading();
      this.alertCtrl.create({
        title: 'Fail.',
        subTitle: 'Password not match. Please try again.',
        buttons: ['Dismiss']
      }).present();
    });
  }

  showLoading() {
    this.loading.present();
  }

  hideLoading() {
    this.loading.dismiss();
  }

}
