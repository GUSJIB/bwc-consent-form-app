import { Injectable } from '@angular/core';
import { Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import * as Constant from '../../configs/constants';
import { HttpInterceptor } from "../httpinterceptor";
import { Storage } from '@ionic/storage';

@Injectable()
export class ApiService {

  apiUrl = Constant.API_ENDPOINT;
  apiTokenKey = Constant.API_TOKEN_KEY;

  constructor(private http: HttpInterceptor, private storage: Storage) {
    this.apiUrl = Constant.API_ENDPOINT;
    this.apiTokenKey = Constant.API_TOKEN_KEY;
  }

  get(path, params?) {
    return new Promise((resolve, reject) => {
      this.storage.get(this.apiTokenKey).then((value) => {
        let headers = new Headers();
        headers.append('Authorization', value);
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');
        this.http.get(this.apiUrl + path, { headers: headers, params: params })
          .map(res => res.json())
          .subscribe(data => {
            resolve(data);
          }, error => {
            reject(error);
          });
      });
    });
  }

  post(path, params) {
    return new Promise((resolve, reject) => {
      this.storage.get(this.apiTokenKey).then((value) => {
        let headers = new Headers();
        headers.append('Authorization', value);
        headers.append('Content-Type', 'application/json');
        this.http.post(this.apiUrl + path, params, { headers: headers })
          .map(res => res.json())
          .subscribe(data => {
            resolve(data);
          }, error => {
            reject(error);
          });
      });
    });
  }

}
