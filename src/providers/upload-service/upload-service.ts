import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import * as Constant from '../../configs/constants';

import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';

@Injectable()
export class UploadService {

  constructor(private http: Http, private storage: Storage, private transfer: FileTransfer) {
    console.log('Hello UploadService Provider');
  }

  private apiUrl = Constant.API_ENDPOINT;
  private apiTokenKey = Constant.API_TOKEN_KEY;

  upload(pathUrl, file, params) {
    return new Promise((resolve, reject) => {

      this.storage.get(this.apiTokenKey).then((value) => {

        const fileTransfer: FileTransferObject = this.transfer.create();
        let uploadOptions: FileUploadOptions = {
            fileKey: 'file',
            fileName: file.substr(file.lastIndexOf('/') + 1),
            //fileName: "Test001.png",
            headers: { 'Authorization': value },
            params: params
        }
        fileTransfer.upload(file, this.apiUrl + pathUrl, uploadOptions)
          .then((data) => {
            // success
            resolve(data.response);
          }, (err) => {
            // error
            reject(err);
          });

      });

    });
  }

}
