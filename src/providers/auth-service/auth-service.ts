import { Injectable } from '@angular/core';
import { Headers, Http, Response, ResponseType, ResponseOptions } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import * as Constant from '../../configs/constants';
import {  HttpInterceptor } from "../httpinterceptor";
import { Observable } from "rxjs/Rx"


@Injectable()
export class AuthServiceProvider {

  private tokenKey = Constant.API_TOKEN_KEY;
  private baseURL = Constant.API_ENDPOINT;
  private x = null;
  constructor(public http: HttpInterceptor, public storage: Storage) {
    
  }

  checkAuthentication() {
    return new Promise((resolve, reject) => {
      this.storage.get(this.tokenKey).then((value) => {
        console.log(value);
        if("" == value || undefined == value) {
          resolve(false);
        }else{
          let headers = new Headers();
          headers.append('Authorization', value);
          headers.append('Accept', 'application/json');
          headers.append('Content-Type', 'application/json');
          this.http.get(this.baseURL + 'users/profile', {headers: headers})
            .subscribe(res => {
              resolve(true);
            }, (err) => {
              reject(false);
            });
        }
      });
    });
  }

  login(credentials) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.post(this.baseURL + 'login', JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          console.log(res);
          let headers = res.headers;
          this.storage.set(Constant.API_TOKEN_KEY, headers.get('authorization'));
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  signup(credentials) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.post(this.baseURL + 'users/sign-up', JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          resolve(res.json());  
        }, (err) => {
          reject(err);
        });
    });
  }

  confirmPassword(credentials) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.post(this.baseURL + 'users/confirm-password', JSON.stringify(credentials), {headers: headers})
        .subscribe((res : Response) => {
          resolve(res.json());
        }, (err) => {
          reject(err);
        });
    });
  }

  logout() {
    return new Promise((resolve, reject) => {
      this.storage.remove(this.tokenKey).then((res) => {
        resolve(true);
      }, (err) => {
        reject(false);
      });
    });
  }

}
