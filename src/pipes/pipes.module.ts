import { NgModule } from '@angular/core';
import { ImagePipe } from './../pipes/image/image';
import { SafePipe } from './../pipes/safe/safe';
import { OrderByPipe } from './../pipes/order-by/order-by';
@NgModule({
	declarations: [ImagePipe,
    SafePipe,
    OrderByPipe],
	imports: [],
	exports: [ImagePipe,
    SafePipe,
    OrderByPipe]
})
export class PipesModule {}
