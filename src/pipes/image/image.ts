import { Pipe, PipeTransform } from '@angular/core';
import { Http, RequestOptions, Headers, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { Storage } from '@ionic/storage';
import * as Constant from '../../configs/constants';

@Pipe({
  name: 'image',
})
export class ImagePipe implements PipeTransform {
  private apiTokenKey = Constant.API_TOKEN_KEY;
  private apiToken = '';
  constructor(private http: Http, private storage: Storage) {
    this.storage.get(this.apiTokenKey).then((value) => {
      this.apiToken = value;
    });
  }
  /**
   * Takes a value and makes it lowercase.
   */
  transform(url: string, token:any) {
    let headers = new Headers();
    headers.append('Content-Type', 'image/*');
    headers.append('Authorization', token);
    //const headers = new Headers({'Authorization': token, 'Content-Type': 'image/*'}); /* tell that XHR is going to receive an image as response, so it can be then converted to blob, and also provide your token in a way that your server expects */
    return this.http.get(url, new RequestOptions({ headers: headers, responseType: ResponseContentType.Blob })) // specify that response should be treated as blob data
    .map(response => response.blob()) // take the blob
    .switchMap(blob => {
      // return new observable which emits a base64 string when blob is converted to base64
        return Observable.create(observer => {
          const  reader = new FileReader();
          reader.readAsDataURL(blob); // convert blob to base64
          reader.onloadend = function() {
            observer.next(reader.result); // emit the base64 string result
          }
      });
    });
  }
}
